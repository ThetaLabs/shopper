<?php
namespace App\Http\Controllers;

use App\Models\Tracker;
use Illuminate\Http\Request;

class TrackersController extends Controller
{
    public function index()
    {
        $trackers = Tracker::where('user_id', $this->user->id)->get();

        return view('trackers.index', compact('trackers'));
    }

    public function create(Request $request)
    {
        $tracker = new Tracker;
        $tracker->user_id = $this->user->id;
        return $this->update($tracker, $request);
    }

    public function update(Tracker $tracker, Request $request)
    {
        if ($request->isMethod('POST')) {
            return $this->save($tracker, $request);
        }

        return view('trackers.update', compact('tracker'));
    }

    public function save(Tracker $tracker, Request $request)
    {
        $tracker
            ->fill($request->only('url', 'title'))
            ->save();

        return redirect()->route('trackers.update', $tracker->id)
            ->with('success', 'Saved the tracker configuration');
    }

    public function delete(Tracker $tracker)
    {
        $tracker->delete();

        return redirect()->back()
            ->with('success', 'Deleted the tracker successfully');
    }
}
