<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracker extends Model
{
    use HasFactory;

    const TYPE_STOCK = 'stock', TYPE_PRICE = 'price', TYPE_BOTH = 'both';
    const FREQUENCY_DAILY = 'daily', FREQUENCY_HOURLY = 'hourly', FREQUENCY_5MIN = '5min';

    protected $guarded = ['id', 'user_id', 'created_at', 'updated_at'];
    protected $attributes = [
        'type' => self::TYPE_BOTH,
        'frequency' => self::FREQUENCY_HOURLY,
    ];
}
