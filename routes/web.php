<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Route::middleware(['auth:sanctum', 'verified'])->group(function() {
    Route::view('/dashboard', 'dashboard')->name('dashboard');

    Route::get('/trackers', [Controllers\TrackersController::class, 'index'])->name('trackers');
    Route::get('/trackers/create', [Controllers\TrackersController::class, 'create'])->name('trackers.create');
    Route::post('/trackers/create', [Controllers\TrackersController::class, 'create']);
    Route::get('/trackers/{tracker}', [Controllers\TrackersController::class, 'update'])->name('trackers.update');
    Route::post('/trackers/{tracker}', [Controllers\TrackersController::class, 'save']);
});
