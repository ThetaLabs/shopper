<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index();
            $table->foreignId('product_id')->index()->nullable();
            $table->string('type', 40); // stock, price, both
            $table->string('frequency', 40); // daily, hourly, 5min
            $table->string('url');
            $table->string('title')->nullable();
            $table->string('mpn')->nullable();
            $table->string('upc', 15)->nullable();
            $table->decimal('price')->nullable();
            $table->string('status')->nullable(); // in-stock, out-of-stock, backordered, discontinued
            $table->timestamps();
            $table->timestamp('last_scraped_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackers');
    }
}
